import os
import datetime as dt
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def date_parser(d_bytes):
    s = d_bytes.decode('utf-8')
    return dt.datetime.strptime(s, '%Y-%m-%d %H:%M:%S')
    return np.datetime64(dt.datetime.strptime(s, '%Y-%m-%d %H:%M:%S'))

mode="single" #single (all days in one) or all (contiguous days)

TIMEPOS  = 0
MILLISPOS= 1
HUMPOS   = 2
TEMPPOS  = 3
nrows    = 3
ncols    = 2
nstart   = 1

#Retrieve figures
files=os.listdir()
files=[f for f in files if ".CSV" in f]
f=files[0]

#Plot
w = 7
h = 11
plt.figure(figsize=[w, h])
for fi, f in enumerate(files[nstart:nstart+nrows]):
    print(f)
    data=np.genfromtxt(f, delimiter=',', converters={0: date_parser})
    tdata=[d[TIMEPOS] for d in data]
    if mode=="single":
        tdata=[t.replace(year=2020, day=1, month=1) for t in tdata]
        midnight=tdata[0].replace(hour=0, minute=0, second=0, microsecond=0)
        tdata=[(t-midnight).seconds/3600 for t in tdata]
    tdata   =np.array(tdata)/24.0*2*np.pi #Rescale to angles for polar plot
    hdata   =[ d[HUMPOS] for d in data]
    tempdata=[d[TEMPPOS] for d in data]

    #Humidity
    plt.subplot(nrows, ncols, 2*fi+1, polar=True)
    plt.gca().set_theta_zero_location("N")  # theta=0 at the top
    plt.gca().set_theta_direction(-1)       # theta increasing clockwise
    plt.polar(tdata, hdata, '-', color='b')
    plt.xlabel("Hour")
    hours =np.array(range(0, 24))
    angles=hours/24.0*2*np.pi
    plt.xticks(ticks=angles, labels=hours)
    plt.gca().set_title("Humidity (%)")
    plt.yticks(list(range(0, 100, 20)))
    #Temperature
    plt.subplot(nrows, ncols, 2*fi+2, polar=True)
    plt.polar(tdata, tempdata, '-', color='r')
    plt.xlabel("Hour")
    plt.xticks(ticks=angles, labels=hours)
    plt.gca().set_title("Temperature (C)")
    plt.yticks(list(range(0, 35, 10)))

#plt.show()
    plt.subplots_adjust(hspace=0.5, top=0.925, bottom=0.05, left=0.05, right=0.95)
plt.savefig("polar.png", dpi=300)
