//DHT Sensor
#include <DHT.h>
//OLED
#include <Wire.h> //communicates with I2C devices
#include "SSD1306Ascii.h" //light version of the adafruit version, with no bufgfer, ie more SRAM space
#include "SSD1306AsciiWire.h"
//RTC
#include "RTClib.h"
//SD
#include <SPI.h>
#include <SD.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
//#define YELLOWHEIGHT 12  // guessing hereheight of yellow layer
//#define BLUEHEIGHT SCREEN_HEIGHT-YELLOWHEIGHT    // 
#define DOUBLETEXTHEIGHT 16 //text height in pixels for double sized font
#define MIDDISPLAY 64 //middle line of the display
#define XOFFSET 3 //offset in the x direction of the oled to center data text
#define XOFFSETTITLES 8 //offset in the x direction of the oled to center data text
#define OLEDBUTTON 4
#define DHTPIN 7
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//LEDS
#define TEMPLED 5 //PWM pins
#define HUMLED 3
//SD
#define CHIPSELECTPIN 10

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
SSD1306AsciiWire display;

// Initialize DHT sensor.
DHT dht(DHTPIN, DHTTYPE);

//Initialize RTC object
RTC_DS1307 rtc;

//Initialize SD objects
const int chipSelect = CHIPSELECTPIN; //as per the DataLogger manual
//File logfile;

void dateTime(uint16_t* date, uint16_t* time) { //callback function for timestamping files in SD card
 DateTime now = rtc.now();
 *date = FAT_DATE(now.year(), now.month(), now.day());
 // return time using FAT_TIME macro to format fields
 *time = FAT_TIME(now.hour(), now.minute(), now.second());
}

class display_handler{
  public:
    display_handler(); //constructor/initialization function, also enumerate inputs
    void update();
    void activate();
    void set_data(float hum, float temp); //pass data from sensor
  private:
    float _hum, _temp=0.0;
    bool _active=false;
    bool _on=false;
    unsigned long _timeoff=5*1000*60UL; //time before turning off, in ms
    unsigned long _tlast=0;
    void _countdown();
    void _render_data();
    void _render_time();
    //int  _centered_xpos(String text);
}; //semicolon eneded here!!!!!!
display_handler::display_handler(){
} //the initialization I wanted didn't work, better yet, pass the required parameters and initialize a display in here
void display_handler::update(){
  if (_active){
    if (!_on){ //only turn off if we are on
      display.ssd1306WriteCmd(SSD1306_DISPLAYON);
      _on=true;
    }
    //display.clear();
    _render_time();
    _render_data();
    _countdown(); //check if we need to turn off
  }  else {
    if (_on){ //only turn off if we are on
      display.ssd1306WriteCmd(SSD1306_DISPLAYOFF);
      _on=false;
    }
  }
}
void display_handler::activate(){
  _active = true;
  _tlast=millis();
}
void display_handler::_countdown(){
  unsigned long tnow=millis();
  if (tnow-_tlast>=_timeoff){
    _active=false;
  }
}
void display_handler::set_data(float hum, float temp){
  _hum=hum;
  _temp=temp;
}
void display_handler::_render_data(){
  display.set2X();             // Draw 2X-scale text
  display.print("HUMD "); display.println(_hum);
  display.print("TEMP "); display.println(_temp);
}
void display_handler::_render_time(){ //put time at the top of the oled display, in the yellow section
  display.set2X();             // Draw 2X-scale text
  display.setCursor(0,0);             // Start at top-left corner
  char strtime[3];
  DateTime now = rtc.now();
  sprintf(strtime, "%02d", now.hour()); //formatted print to a string
  display.print(strtime);
  display.print(":");
  sprintf(strtime, "%02d", now.minute());
  display.print(strtime);
  display.print(":");
  sprintf(strtime, "%02d", now.second());
  display.println(strtime);
}

display_handler mydisplay = display_handler(); //define a new object from this class

class leddriver{
  public:
    leddriver(float vmin, float vmax, int pin);
    void update(float val);
  private:
    unsigned long _tlast=0;
    int _vmin, _vmax, _pin;
    bool _state=false;
};
leddriver::leddriver(float vmin, float vmax, int pin){
  _vmin=vmin;
  _vmax=vmax;
  _pin=pin;
}
void leddriver::update(float val){
  unsigned long tnow=millis();
  unsigned long tfreq;
  tfreq=1500.0*(_vmax-val)/(_vmax-_vmin) + 250.0; //the equation goes from 1 to 0, not the other way around
  if (tnow-_tlast>=tfreq){
    _state=!_state; //toggle
    _tlast=tnow;
    if (_state){ //if on, pwm
    analogWrite(_pin, 25);
    } else {
      digitalWrite(_pin, LOW);
    }
  }
}

leddriver humleddriver=leddriver(0.0, 100.0, HUMLED);
leddriver templeddriver=leddriver(-5.0, 35.0, TEMPLED);


class logger_handler{
  public:
    logger_handler(); //constructor/initialization function, also enumerate inputs
    void update(float h, float t);
    void begin();
  private:
    unsigned long _timelog=5*60UL; //time before logging data, in secs, NOT MILLIS
    unsigned long _tlast=0;
  }; //semicolon needed here!!!!!!
logger_handler::logger_handler(){}
void logger_handler::begin(){
  DateTime now = rtc.now();
  _tlast=now.unixtime(); //in seconds since epoch
}
void logger_handler::update(float h, float t){
  DateTime now = rtc.now();
  if (now.unixtime()-_tlast<=_timelog) { //early exit
    return;
    }
  //else
  _tlast=now.unixtime();
  char filename[13];
  File logfile;
  sprintf(filename, "%02d%02d%02d.CSV", now.year(), now.month(), now.day()); //formatted print to a string
  //reverse order date sorts correctly numerically
  //modulo 1000 for the year truncates to last two digits, eg 2020 -> 20
  //actually, have enough place not to truncate year
  SD.begin(chipSelect); //toggle SD card for hot swapping, problems?
  //this doesn't overwrite file, just opens for appending
  logfile = SD.open(filename, FILE_WRITE); //this doesn't return false if card has been removed, for some reason
  //date in YYYY-MM-DD HH:MM:SS, millis, h, t
  logfile.print(now.year());
  logfile.print("-");
  logfile.print(now.month());
  logfile.print("-");
  logfile.print(now.day());
  logfile.print(" ");
  logfile.print(now.hour());
  logfile.print(":");
  logfile.print(now.minute());
  logfile.print(":");
  logfile.print(now.second());
  logfile.print(",");

  logfile.print(millis());
  logfile.print(",");
  logfile.print(h);
  logfile.print(",");
  logfile.println(t);
  logfile.close();
  }
logger_handler mylogger;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Setup started");
  pinMode(OLEDBUTTON, INPUT_PULLUP);

  //DHT
  dht.begin();

  //LEDS
  pinMode(TEMPLED, OUTPUT);
  pinMode(HUMLED, OUTPUT);
  
  //OLED
  Wire.begin();
  Wire.setClock(400000L);
  //initialize display
  display.begin(&Adafruit128x64, 0x3C);// Address 0x3C for 128x64 as obtained from 12c scanner
  //the default adafruit library uses a 1kb internal ram buffer for drawing routines, this plus SD libraries use too much memory and malloc fails when initializing
  //the OLED objects. Solution: Use ssd ascii which is only for text routines, much less memory required.
  display.setFont(Adafruit5x7);
  display.clear();
  display.ssd1306WriteCmd(SSD1306_DISPLAYOFF); // To switch display off
  display.ssd1306WriteCmd(SSD1306_DISPLAYON);
  mydisplay.activate();

  //RTC
  rtc.begin();
  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  //SD
  pinMode(CHIPSELECTPIN, OUTPUT);
  SdFile::dateTimeCallback(dateTime); //associate callback timestamp function
  SD.begin(chipSelect);
  Serial.println("Setup finished");

  //Other
  mylogger.begin(); //need to do here, if we do in made code rtc is not yet initialized and freezes
}

void loop() {
  int i;
  i=digitalRead(OLEDBUTTON);
  if (i==0){
    mydisplay.activate();
  }

  //DHT
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  //check for errors
  if (isnan(h) || isnan(t)) {
    return;
  }
  mydisplay.set_data(h, t);

  humleddriver.update(h);
  templeddriver.update(t);
  
  mydisplay.update();
  mylogger.update(h, t);
  delay(2000);
}
