# Arduino Humidity and Temperature Logger

A simple(-ish) Arduino UNO based air humidity and temperature logger.

# Introduction

![breadboard](images/logger_bb.png "Red: +5V, Black: Ground, Yellow: Signals, Green: I2C Clock, Pink and Blue: LED Controls")

This project implements an air humidity and temperature logger on an Arduino UNO board, with a real time clock (RTC) and SD card logger shield, and an OLED screen output. Includes source code, breadboard connection diagram, bill of materials, and expected example data with plots.

# Bill of Materials

| Item                 | Notes |
| ------               | ------ |
| Arduino UNO          |  |
| Data Logger Shield   | Should provide a real time clock and SD card reader/writer |
| Breadboard           ||
| DHT22                | Humidity and temperature sensor |
| OLED Display         | 128x64 pixels, I2C protocol, SSD1306 driver compatible|
| Push Button          | Toggles OLED screen on |
| 2 LEDs               | Read anb Blue for aesthetics, optional |
| 2 resistors          | For the LEDs, default 220 $`\Omega`$ |

# Notes

* OLED Library: The usually recommended Adafruit SSD1306 library does not work here, since the library requires a large amount of SRAM memory for a screen buffer. This conflcits with the RTC and/or SD libraries, which also require quite a bit of SRAM. One of the related objects will fail to create, and the Arduino will hang. Check that objects are initialized correctly when debugging! To avoid this, the SSD1306Ascii library is used, which is much less flexible but also does not initialize an internal buffer.
* OLED Voltage: Tutorials and datasheets usually mention that the OLED screen should be powered and driven by 3.3V signals. This is not so simple with the Arduino UNO (other Arduino boards operate off of 3.3V logic levels), but it hasn't affected my screen so far. It might make the OLED degrade faster. To avoid this, the screen is also programmed to shut off after a certain time, and be toggled on by the push button.
* Data Logger Shield: The shield sits on top of the Arduino UNO and connects to most of the Arduino pins. For this reason I am not sure which pins control the Shield, but from some searching this version seems to use the I2C signals (SCL and SDA) plus some of the digital pins (10 thru 13). Breadboard view shows the data logger to the side for ease of representation/wiring.
* SD Card Quirks: The data logger shield does have some pins to detect whether an SD card is present and ready, which can be connected to Arduino pins to perform sanity checks before attempting to write to the card. This requires some soldering unfortunately. Instead I have opted to just toggle the SD card each time before writing, which seems to work correctly when removing and reinserting the SD card during live operation. This might degrade the life span of the SD card, but I doubt it.

# Logging

The device logs the temperature and humidity into a simple text file, with one file per day, and one record taken every approx. 5 minutes (after startup time). Time is tracked with the RTC. File names are in year-month-day order, so they will sort correctly when sorted alphabetically.

A data line looks like:

```
2020-7-22 0:4:29,602205,57.00,22.60
```

Which is to be interpreted as:

```
2020-7    -22  0   :4     :29    ,602205,57.00      ,22.60
year-month-day hour:minute:second,millis,humidity(%),temperature(C)
```

Where millis is the time since the device has been turned on, in milliseconds (this is unreliable since it eventually rolls over, but useful to have).

Example files can be found in the [plotting folder.](plotting/)

# Plotting

Plotting the data is somewhat complicated since it is noisy and changes quite a bit with time or day and across seasons. One interesting possibility I've seen recently is utilizing polar plots. An example for data across three consecutive days is the following:

![polarplot](plotting/polar.png "Humidity and temperature across three consecutive days in polar plot fashion.")

Angle of the polar plots indicate time of day, distance from center of the plot indicate magnitude (ie, temperature or humidity).

Temperature seems to remain constant across days, which could be right (since data was taken in spring), but could also indicate that the temperature function of the DHT22 is not working correctly.

Humidity data is more interesting: Humidity increases through the night (from 0 to 8-9 AM) when windows are closed, and drops suddenly in the morning when the room is aired. This could also be a consequence of temperature changes that the sensor fails to detect, since relative air humidity depends also on temperature.
